import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AccountListComponent } from './account/account-list/account-list.component';
import { AccountItemComponent } from './account/account-list/account-item/account-item.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';

import { AccountService } from './services/account.service';
import { DataStorageService } from './services/data-storage.service';
import { TransactionsService } from './services/transactions.service';
import { CategoryService } from './services/category.service';

import { DynamicEditDeleteDirective } from './shared/directives/DynamicEditDelete.directive';

import { FilterPipe } from './shared/pipes/filter.pipe';

import { AccountEditComponent } from './account/account-edit/account-edit.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { TransactionsEditComponent } from './transactions/transactions-edit/transactions-edit.component';
import { TransactionsItemComponent } from './transactions/transactions-item/transactions-item.component';
import { DropdownDirective } from './shared/directives/dropdown.directive';
import { HelperMethods } from './shared/HelperMethods';
import { AuthService } from './services/auth.service';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthWriteGuard } from "./auth/auth-write-guard.service";
import { AuthGuard } from "./auth/auth-guard.service";
import { CategoryListComponent } from './category/category-list/category-list.component';
import { CategoryItemComponent } from './category/category-list/category-item/category-item.component';
import { TransfersComponent } from './transfers/transfers.component';
import { TransferItemComponent } from './transfers/transfer-item/transfer-item.component';
import { TransferService } from './services/transfer.service';
import { TransferEditComponent } from './transfers/transfer-edit/transfer-edit.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CategoryEditComponent } from './category/category-edit/category-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountListComponent,
    AccountItemComponent,
    HeaderComponent,
    AccountEditComponent,
    TransactionsComponent,
    SigninComponent,
    SignupComponent,
    DynamicEditDeleteDirective,
    DropdownDirective,
    FilterPipe,
    TransactionsEditComponent,
    TransactionsItemComponent,
    CategoryListComponent,
    CategoryItemComponent,
    TransfersComponent,
    TransferItemComponent,
    TransferEditComponent,
    CategoryEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    NgbModule    
  ],
  providers: [AccountService, DataStorageService, TransactionsService, TransferService,
              CategoryService, HelperMethods, AuthService, AuthGuard, AuthWriteGuard],
  bootstrap: [AppComponent],
  entryComponents: [
    CategoryEditComponent
    ]
})
export class AppModule { }
