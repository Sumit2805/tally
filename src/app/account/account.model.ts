export class Account {
    public accountId: number;
    public name: string;
    public initial: number;
    public isClosed: boolean;
    public color: string;
    public isCredit: boolean;

    constructor(id: number, name: string, initial: number, isClosed: boolean, color: string, isCredit: boolean) {
        this.accountId = id;
        this.name = name;
        this.initial = initial;
        this.isClosed = isClosed;
        this.color = color;
        this.isCredit = isCredit;
    }
}

// accountId:1
// color:"#B3A2E0"
// initial:46.34999847
// isClosed:false
// isCredit:false
// name:"InHand (Sumit)"