import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Account } from '../account.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-account-edit',
    templateUrl: './account-edit.component.html',
    styleUrls: ['./account-edit.component.css']
})
export class AccountEditComponent implements OnInit, OnDestroy {

    constructor(private accountService: AccountService, public activeModal: NgbActiveModal) { }

    dialogTitle: string = 'New Account'

    isEdit: boolean = false;
    accountForm: FormGroup = null;
    subscription: Subscription;

    ngOnInit() {
        this.initializeForm();
        this.setDialogTitle();
        this.accountService.onAccountCollectionChanged.subscribe((eventData: any) => {
            this.onCancelEdit();
        });
    }

    setDialogTitle() {
        if (this.isEdit) {
            this.dialogTitle = "Edit Account";
        }
        else {
            this.dialogTitle = "New Account";
        }
    }

    initializeForm() {
        if (this.accountForm == null) {
            this.accountForm = new FormGroup({
                'accountid': new FormControl(0),
                'name': new FormControl('', Validators.required),
                'initial': new FormControl(0.00, Validators.required),
                'isClosed': new FormControl(true),
                'isCredit': new FormControl(false),
                'color': new FormControl('')
            });
        }
    }

    initializeEdit(accountID: number) {
        this.isEdit = true;
        this.initializeForm();

        this.accountService.getAccount(accountID).subscribe(
            (account: Account) => {
                this.accountForm.patchValue({
                    'accountid': account.accountId,
                    'name': account.name,
                    'initial': account.initial,
                    'isClosed': account.isClosed,
                    'isCredit': account.isCredit,
                    'color': ''
                });
            });
    }

    onSubmit() {
        this.accountService.addOrUpdateAccount(this.isEdit, this.accountForm.value);
    }

    onCancelEdit() {
        this.activeModal.close();
    }

    ngOnDestroy() {
        
    }
}
