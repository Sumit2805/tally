import { Component, OnInit, Input } from '@angular/core';
import { Account } from '../../account.model';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { TransactionsEditComponent } from 'src/app/transactions/transactions-edit/transactions-edit.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TransferEditComponent } from 'src/app/transfers/transfer-edit/transfer-edit.component';
import { modelGroupProvider } from '@angular/forms/src/directives/ng_model_group';
import { AccountEditComponent } from '../../account-edit/account-edit.component';
import { Observable, Observer } from 'rxjs';

@Component({
    selector: 'app-account-item',
    templateUrl: './account-item.component.html',
    styleUrls: ['./account-item.component.css']
})
export class AccountItemComponent implements OnInit {

    constructor(private accountService: AccountService,
        private router: Router,
        private modalService: NgbModal) { }

    @Input('account') account: Account;
    @Input('index') itemIndex: number;

    public accountBalance : Observable<any>;

    ngOnInit() {
        this.accountBalance = this.accountService.getAccountBalance(this.account.accountId);
    }

    makeTransaction(doIncomeTransaction: boolean) {
        const modalRef = this.modalService.open(TransactionsEditComponent);
        modalRef.componentInstance.IsIncomeTransaction = doIncomeTransaction;        
        modalRef.componentInstance.initializeNew(this.account.accountId, -1);
    }

    makeTransfer(transferFrom: boolean) {
        const modalRef = this.modalService.open(TransferEditComponent);
        let fromAccount: number = -1, toAccount: number = -1;
        if (transferFrom) fromAccount = this.account.accountId;
        if (!transferFrom) toAccount = this.account.accountId;
        modalRef.componentInstance.initializeNew(fromAccount, toAccount);
    }

    onDelete() {
        var confirmMsg = "Do you really want to delete '" + this.account.name + "' account?"
        var r = confirm(confirmMsg);
        if (r == true) {
            this.accountService.deleteAccount(this.account.accountId);
        }
    }

    onEdit() {
        const modalRef = this.modalService.open(AccountEditComponent);
        modalRef.componentInstance.initializeEdit(this.account.accountId);
    }

    getClass() {
        if (this.account.isCredit) {
            return "creditAccount";
        }
        else {
            if (this.account.isClosed) {
                return "closedAccount";
            }
            else {
                return "activeAccount";
            }
        }
    }

    getAccountStatus() {
        if (this.account.isClosed) {
            return 'Inactive'
        }
        else {
            return 'Active';
        }
    }

    isCreditStatus() {
        if (this.account.isCredit) {
            return 'Credit a/c';
        }
        else {
            return '';
        }
    }
}
