import { Component, OnInit, OnDestroy, Injectable } from '@angular/core';
import { AccountService } from '../../services/account.service';
import { Account } from '../account.model';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AccountEditComponent } from '../account-edit/account-edit.component';

@Component({
    selector: 'app-account-list',
    templateUrl: './account-list.component.html',
    styleUrls: ['./account-list.component.css']
})

export class AccountListComponent implements OnInit, OnDestroy {

    constructor(private accountService: AccountService,
        private modalService: NgbModal) { }

    activeAccounts: Account[] = new Array();
    inactiveAccounts: Account[] = new Array();

    ngOnInit() {
        this.initializeAccounts();
        this.accountService.onAccountCollectionChanged.subscribe((eventData: any) => {
            this.initializeAccounts();
        });
    }

    initializeAccounts() {
        this.accountService.getAccountsAsync().subscribe((accounts: Account[]) => {
            this.inactiveAccounts.splice(0, this.inactiveAccounts.length);
            this.activeAccounts.splice(0, this.activeAccounts.length);

            accounts.forEach(element => {
                if (element.isClosed) {
                    this.inactiveAccounts.push(element);
                }
                else {
                    this.activeAccounts.push(element);
                }
            });
        });
    }

    ngOnDestroy() {
    }

    newAccount() {
        this.modalService.open(AccountEditComponent);
    }
}