import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString: string, propName: string): any {

    if (value == null) return;

    //filterString = 'TaxWithholding';
    if (value.length === 0 || filterString === '' || filterString === undefined) {
      return value;
    }
    const resultArray = [];
    for (const item of value) {
      if (item[propName].indexOf(filterString) >= 0) {
        resultArray.push(item);
      }
    }
    return resultArray;
  }
}
