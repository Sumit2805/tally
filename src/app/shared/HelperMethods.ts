import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable()
export class HelperMethods {
    
    getTodaysDate() {
        const today = new Date();
        const month = today.getMonth() + 1;
        let monthStr = '';

        if (month < 10) {
            monthStr = '0' + month;
        }

        const todayStr = today.getFullYear() + '-' + monthStr + '-' + today.getDate();
        //const firstDayStr = today.getFullYear() + '-0' + month + '-01';
        return todayStr;
    }

    isValidAmount(control: FormControl): {[s: string]: boolean} {
        let amount = +control.value;
        if (amount == NaN || amount == null) {
            return {'invalidAmount': true};
        }
        else if (amount == 0 && control.dirty) {
            return {'invalidAmount': true};
        }
        return null;
    }
}