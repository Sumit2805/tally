import { Directive,   OnInit,   ElementRef,
        Renderer2, HostListener, HostBinding, Input, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../../services/account.service';
import { TransactionsService } from '../../services/transactions.service';
import { AuthService } from '../../services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TransferService } from 'src/app/services/transfer.service';
  
  @Directive({
    selector: '[appDyEditDelete]'
  })
  export class DynamicEditDeleteDirective {
  
    //Account Service not used here, but when delete() is called it is required in account-list component
    constructor(private elementRef: ElementRef,
                private renderer: Renderer2,
                private router: Router,
                private accountService: AccountService,
                private transactionService : TransactionsService,
                private transferService  : TransferService,
                private modalService: NgbModal,
                private authService: AuthService) {
    }
  
  @Input('onEdit') actionOnEdit: Function;
  @Input('onDelete') actionOnDelete: Function;
  @Input('param') actionParam: string = '';
  
    @HostBinding('style.color') foreColor: string = 'black';
  
    editButton;
    deleteButton;
    commandDiv;
  
    hasInitialized = false;

      ngOnInit() {

      }

      initButtons() {
        if (!this.hasInitialized)
        {
          this.initEditButton();
          this.initDeleteButton();
          this.hasInitialized = true
        }
      }

      buttonStyle: String = "margin-right: 10px; padding-top: 0px; padding-bottom: 0px; font-size: small; ";

      initEditButton() {
        //this.editButton = this.renderer.createElement('button');
        //this.editButton.innerText = "Edit";
        //this.editButton.className = "btn btn-primary";
        //this.editButton.style = this.buttonStyle;
        this.editButton = this.renderer.createElement('i');
        this.editButton.className = "fa fa-edit fa-2x";
        this.editButton.style = "margin-right: 10px; padding-top: 0px; padding-bottom: 0px;";

        this.renderer.listen(this.editButton, 'click', (event) => {
          this.actionOnEdit(this.actionParam);
        });
      }
  
      initDeleteButton() {
        //this.deleteButton = this.renderer.createElement('button');
        //this.deleteButton.innerText = "Delete";
        //this.deleteButton.className = "btn btn-danger";
        //this.deleteButton.style = this.buttonStyle;

        this.deleteButton = this.renderer.createElement('i');
        this.deleteButton.className = "fa fa-trash fa-2x";
        this.deleteButton.style = "margin-right: 10px; padding-top: 0px; padding-bottom: 0px;";

        this.renderer.listen(this.deleteButton, 'click', (event) => {
          this.actionOnDelete(this.actionParam);
        });
      }

      @HostListener('mouseenter') mouseover(eventData: Event) {
        if (!this.isAthenticated()) return;

        this.initButtons();

        this.elementRef.nativeElement.childNodes[0].style.color = "blue";
        this.getChildCommandNode(this.elementRef.nativeElement.childNodes);
        if (this.commandDiv != null) {
          this.renderer.appendChild(this.commandDiv, this.editButton);
          this.renderer.appendChild(this.commandDiv, this.deleteButton);
        }
      }

      getChildCommandNode(childNodes: any) {
        childNodes.forEach(element => {
          if (element.id == 'commandPanel')
          {
            this.commandDiv = element;
            return;
          }            

            if (element.childNodes != null && element.childNodes.length > 0)
              this.getChildCommandNode(element.childNodes);
        });
      }

  
      @HostListener('mouseleave') mouseleave(eventData: Event) {
        if (!this.isAthenticated()) return;
        
        this.elementRef.nativeElement.childNodes[0].style.color = "black";
        if (this.commandDiv != null)
        {
          this.renderer.removeChild(this.commandDiv, this.editButton);
          this.renderer.removeChild(this.commandDiv, this.deleteButton);
        }
      }

      private isAthenticated(): boolean {
          return this.authService.IsAuthenticated() && this.authService.hasWriteAccess();
      }
  }
  