import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Account } from '../../account/account.model';
import { AccountService } from 'src/app/services/account.service';
import * as moment from 'moment';

import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TransferService } from 'src/app/services/transfer.service';
import { Transfer } from '../transfer.model';
import { HelperMethods } from 'src/app/shared/HelperMethods';

@Component({
    selector: 'app-transfer-edit',
    templateUrl: './transfer-edit.component.html',
    styleUrls: ['./transfer-edit.component.css']
})
export class TransferEditComponent implements OnInit {

    //Private
    private isEdit: boolean = false;
    dialogTitle: string = 'New Transfer'

    accounts: Account[];
    transferForm: FormGroup;

    constructor(private helperMethods: HelperMethods,
        public activeModal: NgbActiveModal,
        private accountService: AccountService,
        private transferService: TransferService) { }

    closeResult: string;

    ngOnInit() {
        this.initializeForm();
        this.setDialogTitle();
        this.transferForm.disable();
    }

    initializeForm() {
        this.transferForm = new FormGroup({
            'id': new FormControl(0),
            'transferDate': new FormControl(moment().format("YYYY-MM-DD"), Validators.required),
            'fromId': new FormControl(0, Validators.required),
            'fromAccount': new FormControl(""),
            'toId': new FormControl(0, Validators.required),
            'toAccount': new FormControl(""),
            'amount': new FormControl(0.0, this.helperMethods.isValidAmount.bind(this)),
            'note': new FormControl(""),
        });
    }

    public initializeNew(fromAccount: number = -1, toAccount: number = -1) {
        if (!this.isEdit) {
            this.populateAccounts(fromAccount, toAccount);
        }
    }

    populateAccounts(fromAccount: number, toAccount: number) {
        if (this.accounts == null) {
            this.accountService.getAccountsAsync().subscribe((acs: Account[]) => {
                this.accounts = acs;
                this.patchAccounts(fromAccount, toAccount);
                this.tryEnableForm();
            });
        }
        else {
            this.patchAccounts(fromAccount, toAccount);
        }
    }

    patchAccounts(fromAccount: number, toAccount: number) {
        if (fromAccount == -1) {
            fromAccount = this.accounts[0].accountId;
        }
        if (toAccount == -1) {
            toAccount = this.accounts[1].accountId;
        }
        this.transferForm.patchValue({
            'fromId': fromAccount,
            'toId': toAccount
        });
    }

    setDialogTitle() {
        if (this.isEdit) {
            this.dialogTitle = "Edit Transfer";
        }
        else {
            this.dialogTitle = "New Transfer";
        }
    }

    tryEnableForm() {
        if (this.transferForm.value.accountId != 0) {
            this.transferForm.enable();
        }
    }

    public initializeEdit(transferId: number) {
        this.isEdit = true;
        this.transferService.getTransferById(transferId).subscribe((transfer: Transfer) => {
            if (transfer == null) return;
            this.setDialogTitle();
            this.populateAccounts(transfer.fromId, transfer.toId);
            
            this.transferForm.patchValue({
                'id': transfer.id,
                'transferDate': transfer.transferDate,
                'amount': transfer.amount,
                'note': transfer.note
            });
        }
        );
    }

    onSubmit() {
        this.transferService.addOrUpdateTransfer(this.transferForm.value).subscribe(
            (response) => {
                if (response.status == 201) {
                    this.onCancelEdit();
                }
                else {
                    alert("Transfer failed.");
                }
            },
            (error: Response) => {
                alert("Transfer failed.");
            });
    }

    onCancelEdit() {
        this.activeModal.close();
    }
}
