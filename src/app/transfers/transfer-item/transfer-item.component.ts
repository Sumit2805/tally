import { Component, OnInit, Input } from '@angular/core';
import { Transfer } from '../transfer.model';

@Component({
  selector: 'app-transfer-item',
  templateUrl: './transfer-item.component.html',
  styleUrls: ['./transfer-item.component.css',
  '../../app.component.css']
})
export class TransferItemComponent implements OnInit {

  @Input('transfer') transfer: Transfer;
  @Input('index') index: number;

  constructor() { }

  ngOnInit() {
  }

  getClass() {
    if (this.index % 2 == 0)  {
      return "row col-lg-12 tableRowOdd";
    }
    else {
      return "row col-lg-12 tableRowEven";
    }
  }
}
