export class Transfer {
    public id: number;
    public fromId: number;
    public toId: number;
    public fromAccount: string;
    public toAccount: string;
    public amount: number;
    public transferDate: string;
    public note: string

    constructor(id: number, fromId: number, toId: number, 
        fromAccount: string, toAccount: string, 
            amount: number, transferDate: string, note: string) {
        this.id = id;
        this.fromId = fromId;
        this.toId = toId;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
        this.transferDate = transferDate;
        this.note = note;
    }
}