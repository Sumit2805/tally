import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AccountService } from '../services/account.service';
import { FormGroup, FormControl } from '../../../node_modules/@angular/forms';
import { Transfer } from './transfer.model';
import { TransferService } from '../services/transfer.service';
import { Subscription } from '../../../node_modules/rxjs';
import { Account } from '../account/account.model';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TransferEditComponent } from './transfer-edit/transfer-edit.component';

@Component({
    selector: 'app-transfers',
    templateUrl: './transfers.component.html',
    styleUrls: ['./transfers.component.css']
})
export class TransfersComponent implements OnInit {
    accountObserver = this.accountService.getAccountsAsync();
    accounts: Account[];

    transferForm: FormGroup;
    @ViewChild('searchLabel') lblSearch: ElementRef;

    transfers: Transfer[];
    transactionCount: number;
    private AnyAccount = new Account(-999, '  --- Any ---', 0, false, '', false);

    constructor(private accountService: AccountService, private transferService: TransferService,
        private modalService: NgbModal) { }

    ngOnInit() {
        this.transferForm = new FormGroup({
            'fromAccount': new FormControl(0),
            'toAccount': new FormControl(0),
            'startDate': new FormControl(Date()),
            'endDate': new FormControl(Date()),
        });

        this.accountObserver.subscribe((values: Account[]) => {
            values.splice(0, 0, this.AnyAccount);
            this.accounts = values;
            this.transferForm.enable();
        });

        const firstDayStr = moment().format("YYYY-MM-[01]");
        const todayStr = moment().format("YYYY-MM-DD");

        this.transferForm.setValue({
            'fromAccount': this.AnyAccount.accountId,
            'toAccount': this.AnyAccount.accountId,
            'startDate': firstDayStr,
            'endDate': todayStr,
        })
        this.onSubmit();
    }

    onSubmit() {
        this.lblSearch.nativeElement.innerText = "Searching...";
        this.subscribeTransfersChanged();

        let startDate = this.transferForm.value.startDate;
        let endDate = this.transferForm.value.endDate;
        let fromAccount: number;
        if (this.transferForm.value.fromAccount != this.AnyAccount.accountId)
            fromAccount = this.transferForm.value.fromAccount;

        let toAccount: number;
        if (this.transferForm.value.toAccount != this.AnyAccount.accountId)
            toAccount = this.transferForm.value.toAccount;

        this.transferService.getTransferBetweenDates(startDate, endDate, fromAccount, toAccount);
    }

    subscribeTransfersChanged() {
        this.transferService.onTransfersReceived.subscribe(
            (transfers: Transfer[]) => {
                this.transfers = transfers;
                this.lblSearch.nativeElement.innerText = "(" + this.transfers.length + " transfers found)";
            }
        );
    }

    newTransfer() {
        const modalRef = this.modalService.open(TransferEditComponent);
        modalRef.componentInstance.initializeNew();
    }

    onTransferDelete(id: any) {
        var r = confirm("Do you really want to delete the transfer?" + id);
        if (r == true) {
            this.transferService.deleteTransfer(id);
        }
    }

    onTransferEdit(id: any) {
        const modalRef = this.modalService.open(TransferEditComponent);
        modalRef.componentInstance.initializeEdit(id);
    }
}