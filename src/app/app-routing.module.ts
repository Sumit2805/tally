import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { AccountListComponent } from "./account/account-list/account-list.component";
import { AccountEditComponent } from "./account/account-edit/account-edit.component";
import { TransactionsComponent } from "./transactions/transactions.component";
import { TransactionsEditComponent } from "./transactions/transactions-edit/transactions-edit.component";
import { SigninComponent } from "./auth/signin/signin.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { AuthWriteGuard } from "./auth/auth-write-guard.service";
import { AuthGuard } from "./auth/auth-guard.service";
import { CategoryListComponent } from "./category/category-list/category-list.component";
import { TransfersComponent } from "./transfers/transfers.component";
import { TransferEditComponent } from "./transfers/transfer-edit/transfer-edit.component";

const appRoutes: Routes = [    
    { path: '', redirectTo:'/account-list', pathMatch: 'full' },
    { path: 'signin', component: SigninComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'account-list', component: AccountListComponent, canActivate: [AuthGuard] },
    { path: 'account-list/new', component: AccountEditComponent, canActivate: [AuthWriteGuard] },
    { path: 'account-list/:id/edit', component: AccountEditComponent, canActivate: [AuthWriteGuard] },

    { path: 'category-list', component: CategoryListComponent, canActivate: [AuthGuard] },

    { path: 'transactions', component: TransactionsComponent, canActivate: [AuthGuard] },
    { path: 'transactions/new', component: TransactionsEditComponent, canActivate: [AuthWriteGuard] },
    { path: 'transactions/:id/edit', component: TransactionsEditComponent, canActivate: [AuthWriteGuard] },

    { path: 'transfers', component: TransfersComponent, canActivate: [AuthGuard] },
    { path: 'transfers/new', component: TransferEditComponent, canActivate: [AuthGuard] },
]

@NgModule({
    imports: 
    [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
    
}