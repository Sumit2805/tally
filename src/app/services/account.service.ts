import { Account } from '../account/account.model';
import { DataStorageService } from './data-storage.service';
import { Subject, Observable, Observer } from "rxjs";
import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class AccountService {

    public Accounts: Account[]

    @Output('onAccountCollectionChanged') onAccountCollectionChanged = new EventEmitter<any>();

    constructor(private dataService: DataStorageService) { }

    ngOnInit() {
        this.updateLocalCacheAsync();
    }

    addOrUpdateAccount(isEditMode: boolean, newAccount: Account) {
        this.dataService.addOrUpdateAccounts(isEditMode, newAccount).subscribe(
            (response) => {
                if (response.status == 201) {
                    this.updateLocalCacheAsync();
                    this.onAccountCollectionChanged.emit();
                }
            },
            (error: any) => {
                alert(error);
            }
        )
    }

    deleteAccount(accountId: number) {
        this.dataService.deleteAccounts(accountId).subscribe(
            (response) => {
                this.updateLocalCacheAsync();
                this.onAccountCollectionChanged.emit();
            }),
            (error: any) => {
                alert(error);
            }
    }

    getAccountsAsync(): Observable<Account[]> {
        return this.dataService.getAccounts()
    }

    getAccount(accountId: number): Observable<Account> {
        if (this.Accounts == null) {
            return this.dataService.getAccount(accountId);
        }
        else {
            return Observable.create((observer: Observer<Account>) => {
                observer.next(this.Accounts.find(a => a.accountId === accountId));
            });
        }
    }

    getAccountBalance(accountId: number) {
         return this.dataService.getAccountBalance(accountId);
    }
    
    updateLocalCacheAsync() {
        this.dataService.getAccounts().subscribe(
            (accounts: Account[]) => {
                this.Accounts = accounts.slice();
            }
        )
    }
}