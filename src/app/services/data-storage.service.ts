import { Injectable } from "@angular/core";
import { Http, Response, Headers, ResponseOptions } from "@angular/http";
//import { RecipeService } from "../recipes/recipe.service";
//import { Recipe } from "../recipes/recipe.model";
import 'rxjs/Rx';
import { Subscription } from 'rxjs';
//import { AccountService } from "../account/account.service";
import { Account } from '../account/account.model';
import { AuthService } from "./auth.service";
import { Transaction } from "../transactions/transaction.model";
import { Transfer } from "../transfers/transfer.model";
import { Category } from "../category/category.model";
import { environment } from "src/environments/environment";

@Injectable()
export class DataStorageService { 

    constructor(private http: Http){}

    authToken: any;
    
    //private baseUrl = 'http://localhost:8080/';
    private baseUrl = environment.endpoint;
    private accountsApiUrl = this.baseUrl + 'api/accounts/';
    private accountsummaryApiUrl = this.baseUrl + 'api/accountsummary/';
    private transactionApiUrl = this.baseUrl + 'api/transactions/';
    private transferApiUrl = this.baseUrl + 'api/transfers/';
    private categoryApiUrl = this.baseUrl + 'api/category/';
    private authenticationApiUrl = this.baseUrl + 'api/login';

    private getHeaders() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.authToken);
        return headers;
    }
    
    /* AUTHENTICATION STORAGE */

    loginUser(username: string, password: string) {
        let obj = 
        {
            "username" : username,
            "password" : password
        }

        return this.http.post(this.authenticationApiUrl, obj).map(
            (response: Response) => response.json());
    }

    /* ACCOUNT STORAGE */

    addOrUpdateAccounts(isEditMode: boolean, newAccount: Account) {
        const headers = this.getHeaders();

        if (isEditMode)
        {
            return this.http.put(this.accountsApiUrl + newAccount.accountId, newAccount, { headers : headers });
        }
        else
        {
            return this.http.post(this.accountsApiUrl, newAccount, { headers : headers });
        }
    }

    getAccounts() {
        // if (all) {
        //     this.accountsApiUrl += '?all=true'
        // }
        return this.http.get(this.accountsApiUrl)
        .map((response: Response) => {
            return response.json();
        });
    }

    getAccount(accountID) {
        return this.http.get(this.accountsApiUrl + accountID)    
        .map((response: Response) => {
            return response.json();
        });
    }

    deleteAccounts(accountID: number) {
        const headers = this.getHeaders();
        return this.http.delete(this.accountsApiUrl + accountID, {headers : headers})
        .map((response: Response) => {
            return response.json();
        },
            (error: any) => {
                return error;
            }
        );
    }

    getAccountBalance(accountID) {
        return this.http.get(this.accountsummaryApiUrl + accountID)
        .map((response: Response) => {
            return response.json();
        },
        (error: any) => {
            return error;
        });
    }

    /* TRANSACTION STORAGE */

    addOrUpdateTransaction(isEditMode: boolean, transaction: Transaction) {
        const headers = this.getHeaders();

        if (isEditMode)
        {
            return this.http.put(this.transactionApiUrl + transaction.transactionId, transaction, { headers : headers });
        }
        else
        {
            return this.http.post(this.transactionApiUrl, transaction, { headers : headers });
        }
    }

    getTransactionsBetweenDates(startDate: String, endDate: string, onlyDebit: boolean) {
        //?startDate=01-01-2018&endDate=10-01-2018
        let finalUrl = this.transactionApiUrl + '?startDate=' + startDate + '&endDate=' + endDate;

        if (onlyDebit != null) {
            finalUrl += '&isDebit=' + onlyDebit;
        }

        return this.http.get(finalUrl)    
        .map((response: Response) => {
            return response.json();
        });
    }

    getTransactionsById(transactionId) {

        //transactions/2018
        let finalUrl = this.transactionApiUrl + transactionId;

        return this.http.get(finalUrl)    
        .map((response: Response) => {
            return response.json();
        });
    }

    deleteTransaction(transactionId: number) {
        const headers = this.getHeaders();
        return this.http.delete(this.transactionApiUrl + transactionId, {headers : headers})
        .map((response: Response) => {
            return response.json();
        },
            (error: any) => {
                return error;
            }
        );
    }

     /* CATEGORY STORAGE */

     getCategories(isIncome: boolean) {
        //?isIncome=false
        let finalUrl = this.categoryApiUrl + '?isIncome=' + isIncome;

        return this.http.get(finalUrl)    
        .map((response: Response) => {
            return response.json();
        });
    }

    getCategory(categoryId: number) {
        //?isIncome=false
        let finalUrl = this.categoryApiUrl + categoryId;

        return this.http.get(finalUrl)    
        .map((response: Response) => {
            return response.json();
        });
    }

    addOrUpdateCategory(isEditMode: boolean, newCategory: Category) {
        const headers = this.getHeaders();

        if (isEditMode)
        {
            return this.http.put(this.categoryApiUrl + newCategory.id, newCategory, { headers : headers });
        }
        else
        {
            return this.http.post(this.categoryApiUrl, newCategory, { headers : headers });
        }
    }

    deleteCategory(categoryID: number) {
        const headers = this.getHeaders();
        return this.http.delete(this.categoryApiUrl + categoryID, {headers : headers})
        .map((response: Response) => {
            return response.json();
        },
            (error: any) => {
                return error;
            }
        );
    }

    /* TRANSFER STORAGE */

    getTransfer(transferId: number) {
        let finalUrl = 
            this.transferApiUrl + transferId;
                                
        return this.http.get(finalUrl)    
        .map((response: Response) => {
            return response.json();
        });
    }

    getTransfers(startDate: String, endDate: string, fromAccount: number, toAccount: number) {
        //?startDate=01-01-2018&endDate=10-01-2018
        let finalUrl = 
            this.transferApiUrl + '?fromId=' + fromAccount +
                                '&toId=' + toAccount +
                                '&startDate=' + startDate + 
                                '&endDate=' + endDate;
                                

        return this.http.get(finalUrl)    
        .map((response: Response) => {
            return response.json();
        });
    }

    addOrUpdateTransfer(transfer: Transfer) {
        const headers = this.getHeaders();
        if (transfer.id == 0) {
            return this.http.post(this.transferApiUrl + "new", transfer, { headers : headers });
        }
        else {
            return this.http.put(this.transferApiUrl + "new", transfer, { headers : headers });
        }
    }

    deleteTransfer(transferId: number) {
        const headers = this.getHeaders();
        return this.http.delete(this.transferApiUrl + transferId, {headers : headers})
        .map((response: Response) => {
            return response.json();
        },
            (error: any) => {
                return error;
            }
        );
    }

}
