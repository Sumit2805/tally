import { DataStorageService } from './data-storage.service';
import { Subject, Observable, Observer } from "rxjs";
import { Injectable, EventEmitter, Output } from '@angular/core';
import { Transaction } from '../transactions/transaction.model';

@Injectable()
export class TransactionsService {

    @Output('onTransactionsReceived') onTransactionsReceived = new EventEmitter<Transaction[]>();

    constructor(private dataService: DataStorageService)  {}

    addOrUpdateTransaction(isEditMode: boolean, transaction: Transaction) {
        return this.dataService.addOrUpdateTransaction(isEditMode, transaction);
   }

    getTransactionBetweenDates(startDate: String, endDate: string, onlyDebit: boolean) {
         this.dataService.getTransactionsBetweenDates(startDate, endDate, onlyDebit).subscribe(
            (transactions: Transaction[]) => {
                this.onTransactionsReceived.emit(transactions.slice());
            }
        )
    }

    getTransactionById(transactionID: number): Observable<Transaction> {
            return this.dataService.getTransactionsById(transactionID)
   }

   deleteTransaction(transactionId: number) {
        this.dataService.deleteTransaction(transactionId).subscribe(
        (response) => {            
            
        }),
        (error) => {
            alert(error);
        }
    }
}