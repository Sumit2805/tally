import { DataStorageService } from './data-storage.service';
import { Injectable, EventEmitter, Output } from '@angular/core';
import { Transfer } from '../transfers/transfer.model';

@Injectable()
export class TransferService {

    @Output() onTransfersReceived = new EventEmitter<Transfer[]>();

    constructor(private dataService: DataStorageService) { }


    addOrUpdateTransfer(transfer: Transfer) {
        return this.dataService.addOrUpdateTransfer(transfer);
    }

    getTransferById(transferId: number) {
        return this.dataService.getTransfer(transferId);
    }

    getTransferBetweenDates(startDate: String, endDate: string, fromAccount: number, toAccount: number) {
        this.dataService.getTransfers(startDate, endDate, fromAccount, toAccount).subscribe(
            (transfers: Transfer[]) => {
                this.onTransfersReceived.emit(transfers.slice());
            }
        )
    }

    deleteTransfer(transferId: number) {
        this.dataService.deleteTransfer(transferId).subscribe(
            (response) => {
                return response;
            }),
            (error) => {
                alert(error);
            }
    }
}