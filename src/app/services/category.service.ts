import { DataStorageService } from './data-storage.service';
import { Injectable, OnInit, Output,EventEmitter } from '@angular/core';
import { Category } from '../category/category.model';
import { Observer, Observable } from 'rxjs';
//import { Category } from '../category/category.model';

@Injectable()
export class CategoryService implements OnInit {
    constructor(private dataService: DataStorageService)  {}

    public AllCategories: Category[];

    public IncomeCategories: Category[];
    public ExpenseCategories: Category[];

    @Output('onCategoryCollectionChanged') onCategoryCollectionChanged = new EventEmitter<any>();

    ngOnInit() {
        this.InitializeCategories();    
    }
    
    private InitializeCategories() {
        this.dataService.getCategories(null).subscribe(
            (categories: Category[]) => {
                this.AllCategories = categories.slice();
            });

        this.dataService.getCategories(true).subscribe(
            (categories: Category[]) => {
                this.IncomeCategories = categories.slice();
            });

        this.dataService.getCategories(false).subscribe(
            (categories: Category[]) => {
                this.ExpenseCategories = categories.slice();
            });    
    }

    /*
    * Intended to be called from Category component, so to get real (non-catched) data
    */
    getCategoriesAsync(isIncome: boolean): Observable<Category[]>  {
        return this.dataService.getCategories(isIncome);
    }

    getCategoryAsync(categoryId: number): Observable<Category>  {
        return this.dataService.getCategory(categoryId);
    }
    
    addOrUpdateCategory(isEditMode: boolean, newCategory: Category) {
        return this.dataService.addOrUpdateCategory(isEditMode, newCategory).subscribe(
            (response) => {
                if (response.status == 201) {
                    this.InitializeCategories();
                    this.onCategoryCollectionChanged.emit();
                }
            },
            (error: any) => {
                alert(error);
            }
        );
    }

    deleteCategory(categoryID: number) {
        this.dataService.deleteCategory(categoryID).subscribe(
            (response) => {
                this.InitializeCategories();
                this.onCategoryCollectionChanged.emit();
                return response;
            }),
            (error: any) => {
                alert(error);
            }
    }

/*    
    // Get categories synchronously using async-await

    // Get the catched income categories
    async getIncomeCategories(): Promise<Category[]>  {
        if (this.IncomeCategories == null) {
            this.InitializeCategories();
            let expCategories = await this.dataService.getCategories(true).toPromise();
            return expCategories;
        }
        else {
            return this.IncomeCategories.slice();
        }
    }

    // Get the catched expense categories
    async getExpenseCategories(): Promise<Category[]>  {
        if (this.ExpenseCategories == null) {
            this.InitializeCategories();
            let expCategories = await this.dataService.getCategories(false).toPromise();
            return expCategories;
        }
        else {
            return this.ExpenseCategories.slice();
        }
    }
*/


//     getTransactionById(transactionID: number): Observable<Transaction> {
//         if (this.localTransactions == null) {
//             return this.dataService.getTransactionsById(transactionID)
//         }
//         else {            
//             return Observable.create((observer: Observer<Transaction>) => {
//                 let t = this.localTransactions.find(a => a.transactionId === transactionID);
//                 if (t == null) {
//                     observer.error('Transaction not found');
//                 }
//                 observer.next(t);                
//             });
//         }
//    }
}