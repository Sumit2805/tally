import { DataStorageService } from "./data-storage.service";
import { Injectable, Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

@Injectable()
export class AuthService {

    constructor(private dataService: DataStorageService) {}

    private authToken: any;
    private accessRight: any;

@Output() onUserSignedIn = new EventEmitter<string>();

    signinUser(username: string, password: string) {
        this.dataService.loginUser(username, password).subscribe(
            (response: any) => {
                this.authToken = response.token;
                this.accessRight = response.accessright;
                this.dataService.authToken = response.token;
                this.onUserSignedIn.emit(response.statusText);
            }
        )
    }

    getToken() {
        return 0;
    }

    hasWriteAccess() {
        return true;
    }

    signOutUser() {
        this.authToken = null;
        this.dataService.authToken = null;
    }

    IsAuthenticated() {
        return true;
    }

    /*
    // Working Just dont want every time to login during development
    getToken() {
        return this.authToken;
    }

    hasWriteAccess() {
        return this.accessRight == "WriteAccess";
    }

    signOutUser() {
        this.authToken = null;
        this.dataService.authToken = null;
    }

    IsAuthenticated() {
        return this.authToken != null;    
    }
    */
}