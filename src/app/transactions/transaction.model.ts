export class Transaction {
    public transactionId: number;
    public amount: number;
    public categoryId: number;
    public categoryName: string;
    public accountId: number;
    public accountName: string;
    public transactionDate: string;
    public payeeName: string
    public isDebit: boolean;

    constructor(id: number, amount: number,                 
                categoryId: number, categoryName: string, 
                accountId: number, accountName: string, payeeName: string, transactionDate: string, isDebit: boolean) {
        this.transactionId = id;
        this.amount = amount;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.accountId = accountId;
        this.accountName = accountName;
        this.payeeName = payeeName;
        this.transactionDate = transactionDate;
        this.isDebit = isDebit;
    }
}