import { Component, OnInit } from '@angular/core';
import { Transaction } from '../transaction.model';
import { TransactionsService } from '../../services/transactions.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountService } from '../../services/account.service';
import { CategoryService } from '../../services/category.service';
import { Category } from '../../category/category.model';
import { Account } from '../../account/account.model';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { HelperMethods } from 'src/app/shared/HelperMethods';

@Component({
    selector: 'app-transactions-edit',
    templateUrl: './transactions-edit.component.html',
    styleUrls: ['./transactions-edit.component.css']
})
export class TransactionsEditComponent implements OnInit {
    //Public
    public IsIncomeTransaction: boolean = false;    

    //Private
    private isEdit: boolean = false;
    dialogTitle: string = 'New Income Transaction'
    sColor = 'lightcoral';

    accounts: Account[];
    categories: Category[];

    transactionForm: FormGroup;

    constructor(public activeModal: NgbActiveModal,
        private helperMethods: HelperMethods,
        private transactionService: TransactionsService,
        private accountService: AccountService, private categoryService: CategoryService) { }

    async ngOnInit() {
        this.initializeForm();        
        this.setDialogTitle();
        this.transactionForm.disable();
    }

    initializeForm() {
        this.transactionForm = new FormGroup({
            'transactionId': new FormControl(0),
            'transactionDate': new FormControl(moment().format("YYYY-MM-DD")),
            'accountId': new FormControl(0),
            'categoryId': new FormControl(0),
            'amount': new FormControl(0.0, this.helperMethods.isValidAmount.bind(this)),
            'payeeName': new FormControl(''),
            'isDebit': new FormControl(this.IsIncomeTransaction)
        });
    }

    public initializeNew(accountId: number = -1, categoryId: number = -1) {
        if (!this.isEdit) {
            this.populateAndSetAccounts(accountId);
            this.populateAndSetCategories(categoryId);
        }
    }

    public initializeEdit(transactionID: number) {
        this.isEdit = true;

        if (transactionID > 0) {
            this.transactionService.getTransactionById(transactionID).subscribe((tValue: Transaction) => {
                this.IsIncomeTransaction = tValue.isDebit;
                this.setDialogTitle();
                this.populateAndSetCategories(tValue.categoryId);
                this.populateAndSetAccounts(tValue.accountId);

                this.transactionForm.patchValue({
                    'transactionId': tValue.transactionId,
                    'transactionDate': tValue.transactionDate,
                    'amount': tValue.amount.toFixed(2),
                    'payeeName': tValue.payeeName,
                    'isDebit': tValue.isDebit
                });

            });
        }
    }

    setDialogTitle() {
        if (this.isEdit) {
            this.dialogTitle = "Edit";
        }
        else {
            this.dialogTitle = "New";
        }

        if (this.IsIncomeTransaction) {
            this.dialogTitle += " Income";
            this.sColor = "lightgreen";
        }
        else {
            this.dialogTitle += " Expense";
        }
    }
    /*----------------------------------------------------------------------------------------*/

    populateAndSetCategories(selectedCat: number) {
        if (this.categories == null) {
            this.categoryService.getCategoriesAsync(this.IsIncomeTransaction).subscribe((cats: Category[]) => {
                this.categories = cats;
                this.patchCategory(selectedCat);
                this.tryEnableForm();
            });
        }
        else {
            this.patchCategory(selectedCat);
        }
    }

    patchCategory(selectedCat: number) {
        if (selectedCat == -1) {
            selectedCat = this.categories[0].id;
        }
        this.transactionForm.patchValue({
            'categoryId': selectedCat
        });
    }

    /*----------------------------------------------------------------------------------------*/

    populateAndSetAccounts(selectedAccount: number) {
        if (this.accounts == null) {
            this.accountService.getAccountsAsync().subscribe((acs: Account[]) => {
                this.accounts = acs;
                this.patchAccount(selectedAccount);
                this.tryEnableForm();
            });
        }
        else {
            this.patchAccount(selectedAccount);
        }
    }

    patchAccount(selectedAccount: number) {
        if (selectedAccount == -1) {
            selectedAccount = this.accounts[0].accountId;
        }
        this.transactionForm.patchValue({
            'accountId': selectedAccount
        });
    }

    /*----------------------------------------------------------------------------------------*/

    onSubmit() {
        this.transactionForm.disable();
        this.transactionService.addOrUpdateTransaction(this.isEdit, this.transactionForm.value).subscribe(
            (response) => {
                if (response.status == 201) {
                    this.onCancelEdit();
                }
            }
        );
    }

    onCancelEdit() {
        this.activeModal.close();
    }

    tryEnableForm() {
        if (this.transactionForm.value.accountId != 0 &&
            this.transactionForm.value.categoryId != 0) {
            this.transactionForm.enable();
        }
    }
}
