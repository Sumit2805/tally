import { Component, OnInit, Input } from '@angular/core';
import { Transaction } from '../transaction.model';

@Component({
  selector: 'app-transactions-item',
  templateUrl: './transactions-item.component.html',
  styleUrls: ['./transactions-item.component.css',
  '../../app.component.css'
  ]
})
export class TransactionsItemComponent implements OnInit {

  @Input('transaction') trans: Transaction;
  @Input('index') index: number;
  @Input('showSideColumn') showSideColumn: Boolean;
  
  constructor() { }

  getClass() {
    if (this.index % 2 == 0)  {
      return "row col-lg-12 tableRowOdd";
    }
    else {
      return "row col-lg-12 tableRowEven";
    }
  }

  ngOnInit() {
  }

}
