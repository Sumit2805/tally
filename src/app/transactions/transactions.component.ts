import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Transaction } from './transaction.model';
import { TransactionsService } from '../services/transactions.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Input } from '@angular/core';
import { filter } from 'rxjs-compat/operator/filter';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';
import { CategoryService } from '../services/category.service';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TransactionsEditComponent } from './transactions-edit/transactions-edit.component';

@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit, OnDestroy {

    //accounts = this.accountService.getAccountsAsync();
    //categories = this.categoryService.getCategoriesAsync(null);
    public filterdStatus: string = '';

    constructor(private transactionService: TransactionsService,
        private modalService: NgbModal) { }

    transactionsForm: FormGroup;

    transactions: Transaction[];
    transactionCount: number;

    @ViewChild('searchLabel') lblSearch: ElementRef;

    ngOnInit() {
        this.transactionsForm = new FormGroup({
            'startDate': new FormControl(Date()),
            'endDate': new FormControl(Date()),
            'creditdebit': new FormControl('credit')
        });

        const firstDayStr = moment().format("YYYY-MM-[01]");
        const todayStr = moment().format("YYYY-MM-DD");

        this.transactionsForm.setValue({
            'startDate': firstDayStr,
            'endDate': todayStr,
            'creditdebit': 'all'
        })

        this.   onSubmit();
    }

    onSubmit() {
        this.lblSearch.nativeElement.innerText = "Searching...";
        this.subscribeTransactionsChanged();

        let startDate = this.transactionsForm.value.startDate;
        let endDate = this.transactionsForm.value.endDate;
        let creditDebit = this.transactionsForm.value.creditdebit;
        if (creditDebit == 'debit')
            creditDebit = true;
        else if (creditDebit == 'credit')
            creditDebit = false;
        else if (creditDebit == 'all')
            creditDebit = null;

        this.transactionService.getTransactionBetweenDates(startDate, endDate, creditDebit);
    }

    subscribeTransactionsChanged() {
        this.transactionService.onTransactionsReceived.subscribe(
            (transactions: Transaction[]) => {
                this.transactions = transactions;
                this.lblSearch.nativeElement.innerText = "(" + this.transactions.length + " transactions found)";
            }
        );
    }

    showDebitCreditColumn() {
        let creditDebit = this.transactionsForm.value.creditdebit;
        return (creditDebit == 'all');
    }

    ngOnDestroy() {
    }

    onTransactionDelete(id: any) {
        var r = confirm("Do you really want to delete the transaction?" + id);
        if (r == true) {
            this.transactionService.deleteTransaction(id);
        }
    }

    onTransactionEdit(id: any) {
        const modalRef = this.modalService.open(TransactionsEditComponent);
        modalRef.componentInstance.initializeEdit(id);
    }

    openIncome() {
        const modalRef = this.modalService.open(TransactionsEditComponent);
        modalRef.componentInstance.IsIncomeTransaction = true;
        modalRef.componentInstance.initializeNew();
    }

    openExpense() {
        const modalRef = this.modalService.open(TransactionsEditComponent);
        modalRef.componentInstance.IsIncomeTransaction = false;
        modalRef.componentInstance.initializeNew();
    }
}
