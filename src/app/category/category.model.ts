export class Category {
    public id: number;
    public name: string;
    public color: string;
    public icon: number;   
    public isIncome: boolean;

    constructor(id: number, name: string, isIncome: boolean) {
        this.id = id;
        this.name = name;
        this.isIncome = isIncome;
        this.color = '';
        this.icon = 0;
    }
}