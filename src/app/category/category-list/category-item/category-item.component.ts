import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../../category.model';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TransactionsEditComponent } from 'src/app/transactions/transactions-edit/transactions-edit.component';
import { CategoryEditComponent } from '../../category-edit/category-edit.component';
import { CategoryService } from 'src/app/services/category.service';

@Component({
    selector: 'app-category-item',
    templateUrl: './category-item.component.html',
    styleUrls: ['./category-item.component.css']
})
export class CategoryItemComponent implements OnInit {

    @Input('category') category: Category;
    @Input('index') itemIndex: number;

    isMouseOver = false;

    constructor(private categoryService: CategoryService, private modalService: NgbModal) { }

    ngOnInit() {
    }

    getClass() {
        if (this.category.isIncome) {
            return "incomeCategory";
        }
        else {
            return "expenseCategory";
        }
    }

    modalRef: NgbModalRef;
    makeTransaction() {
        this.modalRef = this.modalService.open(TransactionsEditComponent);
        this.modalRef.componentInstance.IsIncomeTransaction = this.category.isIncome;
        this.modalRef.componentInstance.initializeNew(-1, this.category.id);
    }

    onEdit() {
        const modalRef = this.modalService.open(CategoryEditComponent);
        modalRef.componentInstance.initializeEdit(this.category.id);
    }

    onDelete() {
        var confirmMsg = "Do you really want to delete '" + this.category.name + "' category?"
        var r = confirm(confirmMsg);
        if (r == true) {
            this.categoryService.deleteCategory(this.category.id);
        }
    }
}
