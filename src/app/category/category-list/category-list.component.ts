import { Component, OnInit } from '@angular/core';
import { Category } from '../category.model';
import { CategoryService } from '../../services/category.service';
import { CategoryEditComponent } from '../category-edit/category-edit.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-category-list',
    templateUrl: './category-list.component.html',
    styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

    incomeCategoryList: Category[];
    expenseCategoryList: Category[];


    constructor(private categoryService: CategoryService,
        private modalService: NgbModal) { }

    ngOnInit() {
        this.initializeCategories();
        this.categoryService.onCategoryCollectionChanged.subscribe((eventData: any) => {
            this.initializeCategories();
        });
    }

    initializeCategories() {
        this.categoryService.getCategoriesAsync(true).subscribe(
            (categories: Category[]) => {
                this.incomeCategoryList = categories;
            }
        );

        this.categoryService.getCategoriesAsync(false).subscribe(
            (categories: Category[]) => {
                this.expenseCategoryList = categories;
            }
        );
    }

    onNewCategory() {
        this.modalService.open(CategoryEditComponent);
    }
}
