import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { CategoryService } from '../../services/category.service';
import { Category } from '../category.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit, OnDestroy {

    constructor(private categoryService: CategoryService, public activeModal: NgbActiveModal ) { }

    dialogTitle: string = 'New Category'

    isEdit: boolean = false;
    categoryForm: FormGroup = null;

    ngOnInit() {
        this.initializeForm();
        this.setDialogTitle();
        this.categoryService.onCategoryCollectionChanged.subscribe((eventData: any) => {
            this.onCancelEdit();
        });
    }

    setDialogTitle() {
        if (this.isEdit) {
            this.dialogTitle = "Edit Category";
        }
        else {
            this.dialogTitle = "New Category";
        }
    }

    initializeForm() {
        if (this.categoryForm == null) {
            this.categoryForm = new FormGroup({
                'id': new FormControl(0),
                'name': new FormControl('', Validators.required),
                'color': new FormControl(''),
                'isIncome': new FormControl(true),
                'icon': new FormControl(0)
            });
        }
    }

    initializeEdit(categoryID: number) {
        this.isEdit = true;
        this.initializeForm();

        this.categoryService.getCategoryAsync(categoryID).subscribe(
            (category: Category) => {
                this.categoryForm.patchValue({
                    'id': category.id,
                    'name': category.name,
                    'color': '',
                    'isIncome': category.isIncome,
                    'icon': 0
                });
            });
    }

    onSubmit() {
        this.categoryService.addOrUpdateCategory(this.isEdit, this.categoryForm.value);
    }

    onCancelEdit() {
        this.activeModal.close();
    }

    ngOnDestroy() {
       
    }
}
